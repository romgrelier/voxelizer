#include "../include/Engine.h"
/*
#include <vector>
#include <array>
#include <iostream>
#include <vector>
#include <memory>
#include <glm/glm.hpp>
#include <map>
#include <thread>

struct Component{
};

class EntityFactory;

struct Mouvement : public Component{
public:
	glm::vec3 m_position;
	Mouvement(const float x, const float y, const float z) : m_position{x, y, z}{}
};

struct Mesh : public Component{
public:
	std::vector<float> m_vertices;
	std::vector<float> m_normals;
	std::vector<float> m_color;
};

struct CollisionBox : public Component{
public:
	glm::vec3 m_vecMin;
    glm::vec3 m_vecMax;
};

enum e_Component{
	e_Mouvement,
	e_Mesh,
	e_CollisionBox
};

class Entity{
	friend EntityFactory;
private:
	static unsigned int g_id;
	unsigned int m_id;
	bool m_active;

	std::map<e_Component, unsigned int> m_components;

public:
	Entity() : m_id{g_id++}, m_active{true} {
	}

	void update(){

	}

	unsigned int getId() const { return m_id; };
	bool isActive() const { return m_active; };
	void setActive(const bool active) { m_active = active; };
	std::map<e_Component, unsigned int> & getMap() { return m_components; };

};

unsigned int Entity::g_id{0};

class System;
class Entity;
class EntityFactory{
private:
	std::vector<Entity> m_entity;

	std::vector<Mouvement> c_mouvement;
	std::vector<Mesh> c_mesh;
	std::vector<CollisionBox> c_collisionBox;
public:
	EntityFactory(){}

	Entity & createEntity(){
		m_entity.push_back(Entity());
		Entity & e = m_entity.back();

		c_mouvement.push_back(Mouvement(e.getId(), e.getId(), e.getId()));
		e.m_components.insert(std::pair<e_Component, unsigned int>(e_Mouvement, 0));
		c_mesh.push_back(Mesh());
		e.m_components.insert(std::pair<e_Component, unsigned int>(e_Mesh, 1));
		c_collisionBox.push_back(CollisionBox());
		e.m_components.insert(std::pair<e_Component, unsigned int>(e_CollisionBox, 2));

		return m_entity.back();
	}

	std::vector<Mouvement> & getMouvement(){return c_mouvement; }
	std::vector<Mesh> & getMesh(){return c_mesh; }
	std::vector<CollisionBox> & getCollision(){return c_collisionBox; }

	std::vector<Entity> & getEntities(){ return m_entity; };
};

class System{
private:
public:
	virtual ~System(){};
	virtual void update() =0;
};

class DataSystem : public System{
private:
	EntityFactory & m_entityFactory;
public:
	DataSystem(EntityFactory & e) : m_entityFactory{e}{}

	void update() override{
		auto mouv_data = m_entityFactory.getMouvement();
		auto mesh_data = m_entityFactory.getMesh();
		auto collision_data = m_entityFactory.getCollision();

		for(Entity & e : m_entityFactory.getEntities()){
			std::cout << 
				"DataSystem" << 
				mouv_data[e.getMap()[e_Mouvement]].m_position.x << " - " << mouv_data[e.getMap()[e_Mouvement]].m_position.y << " - " << mouv_data[e.getMap()[e_Mouvement]].m_position.z <<
				" | " << 
				e.getMap()[e_Mesh] << 
				" | " <<
				e.getMap()[e_CollisionBox] <<
			std::endl;


		}
	}
};

class RenderSystem : public System{
private:
	EntityFactory & m_entityFactory;
public:
	RenderSystem(EntityFactory & e) : m_entityFactory{e}{}

	void update() override{
		for(Entity & e : m_entityFactory.getEntities()){
			std::cout << 
				"RenderSystem" << 
				e.getMap()[e_Mouvement] << 
				" | " << 
				e.getMap()[e_Mesh] << 
				" | " <<
				e.getMap()[e_CollisionBox] <<
			std::endl;
		}
	}
};

enum e_system{
	e_render = 0,
	e_data,
	e_collisionBox
};

class Engine{
private:
	std::vector<System*> m_system;
	EntityFactory & m_entityFactory;

public:
	Engine(EntityFactory & factory) : m_entityFactory{factory}{

	}

	~Engine(){
		for(System * s : m_system){
			delete s;
		}
	}

	void addSystem(e_system s){
		switch(s){
			case e_render:
				m_system.push_back(new RenderSystem(m_entityFactory));
			break;
			case e_data:
				m_system.push_back(new DataSystem(m_entityFactory));
			break;
			case e_collisionBox:
			break;
		}
	}

	void update(){
		std::vector<std::thread> threads;

		for(System * s : m_system){
			threads.push_back(std::thread(&System::update, s));
		}	

		for(auto & t : threads){
			t.join();
		}
	}
};
*/

/*===================================*/

int main()
{
	EntityFactory factory;
	Engine engine(factory);

	std::cout << "loading systems" << std::endl;

	//engine.addSystem(sys_data);
	engine.addSystem(sys_input);
	engine.addSystem(sys_navigation);
	engine.addSystem(sys_combat);
	engine.addSystem(sys_render);
	//engine.addSystem(sys_ui);

	std::cout << "loading entities" << std::endl;

	factory.createWorld();
	//factory.createControlPoint(50, 0, 50);
	/*
	for(unsigned int i{0}; i < 100; i++){
		factory.createUnit(i+10, 0, i+10);
	}
*/
	factory.createUnit(10, 1, 10);
	factory.createUnit(20, 1, 20);
	factory.createUnit(50, 1, 60);
	factory.createUnit(50, 1, 40);
	factory.createUnit(20, 1, 30);

	std::cout << "running" << std::endl;

	engine.run();

	//factory.testDatabase();

	std::cout << "stop" << std::endl;

	return 0;
}