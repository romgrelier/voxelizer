#ifndef WORLDGENERATOR_H
#define WORLDGENERATOR_H

#include <iostream>
#include <libnoise/noise.h>
#include "noiseutils.h"
#include "ecs/Component.h"

class WorldGenerator
{
public:
    static void generateChunk(Chunk * chunk);
    static void generatePathMap(const Chunk * chunk, PathMap * pathMap);
};

#endif // WORLDGENERATOR_H
