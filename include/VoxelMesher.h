#ifndef VOXELMESHER__H
#define VOXELMESHER__H

#include "Util.h"
#include "ecs/Component.h"

class VoxelMesher{
    public:
    static void generate(Chunk * chunk, Mesh & mesh);
    static void generateUnit(UnitChunk * chunk, Mesh & mesh);

    private:
    static const float size;

    static void pushPX(const unsigned int x, const unsigned int y, const unsigned int z, const glm::vec4 color, Mesh & mesh);
    static void pushNX(const unsigned int x, const unsigned int y, const unsigned int z, const glm::vec4 color, Mesh & mesh);

    static void pushPY(const unsigned int x, const unsigned int y, const unsigned int z, const glm::vec4 color, Mesh & mesh);
    static void pushNY(const unsigned int x, const unsigned int y, const unsigned int z, const glm::vec4 color, Mesh & mesh);

    static void pushPZ(const unsigned int x, const unsigned int y, const unsigned int z, const glm::vec4 color, Mesh & mesh);
    static void pushNZ(const unsigned int x, const unsigned int y, const unsigned int z, const glm::vec4 color, Mesh & mesh);

    static void computeNormals(Mesh & mesh);
};

#endif