#ifndef GLOBAL_H
#define GLOBAL_H

#define CHUNK_SIZE_X 100
#define CHUNK_SIZE_Y 30
#define CHUNK_SIZE_Z 100

#define PI 3.141592654

#endif // GLOBAL_H
