#ifndef ENGINE__H
#define ENGINE__H

#include "ecs/EntityFactory.h"

#include "ecs/RenderSystem.h"
#include "ecs/InputSystem.h"
#include "ecs/NavigationSystem.h"
#include "ecs/CombatSystem.h"
#include "ecs/DataSystem.h"

class InputSystem;
class RenderSystem;
class NavigationSystem;

class Engine{
private:
	std::map<e_system, System*> m_system;
	EntityFactory & m_entityFactory;
	sf::Window * m_window;

public:
	Engine(EntityFactory & factory);
	~Engine();

	sf::Window * getContext();

	void addSystem(const e_system s);
	System * getSystem(const e_system s);

	void update();

	void run();
};

#endif