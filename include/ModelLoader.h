#ifndef MODEL_LOADER__H
#define MODEL_LOADER__H

#include "ecs/Component.h"

class ModelLoader{
public:
	static void loadTank(UnitChunk * chunk);
};

#endif