#ifndef UI_SYSTEM__H
#define UI_SYSTEM__H

#include "System.h"

class UISystem : public System{
    private:
        sf::Window * m_window;
    public:
        UISystem(EntityFactory & entityFactory, Engine & engine, sf::Window * w);
        ~UISystem(){};

        void update() override;
};

#endif