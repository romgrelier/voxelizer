#ifndef ENTITY__H
#define ENTITY__H

#include "Component.h"

#include <map>

class EntityFactory;
class Entity{
	friend EntityFactory;
private:
	static unsigned int g_id;
	unsigned int m_id;
	bool m_active;

	std::map<e_Component, unsigned int> m_components;

public:
	Entity();
	unsigned int getId() const { return m_id; };
	bool isActive() const { return m_active; };
	void setActive(const bool active) { m_active = active; };

	std::map<e_Component, unsigned int> & getMap() { return m_components; };
};

#endif