#ifndef SYSTEM__H
#define SYSTEM__H

#include "EntityFactory.h"

#include <thread>
#include <iostream>

#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>

enum e_system{
	sys_input = 0,
	sys_navigation,
	sys_data,
	sys_collisionBox,
	sys_render,
	sys_combat,
	sys_ui
};

class Engine;

class System{
protected:
	EntityFactory & r_entityFactory;
	Engine & r_engine;
public:
	System(EntityFactory & EntityFactory, Engine & engine);
	virtual ~System(){};

	virtual void update() =0;
};

#endif