#ifndef RENDERSYSTEM__H
#define RENDERSYSTEM__H

#ifndef BUFFER_OFFSET
    #define BUFFER_OFFSET(offset) ((char*)NULL + (offset))
#endif

#include "System.h"

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>

enum RenderMode{
	immediate = 0,
	displayList,
	vertexArray,
	vbo,
	vao
};

class RenderSystem : public System{
private:
	static void render_immediate(const Mesh & mesh);
	void render_displayList(const Mesh & mesh) const;
	void render_vertexArray(const Mesh & mesh) const;
	void render_vbo(const Mesh & mesh) const;
	void render_vao(const Mesh & mesh) const;

	void setCamera(const Camera & target) const;
 
	RenderMode renderMode;
	sf::Window * m_context;
public:
	RenderSystem(EntityFactory & entityFactory, Engine & engine, sf::Window * w);

	~RenderSystem();

	static void loadInDisplayList(Mesh & mesh);
	static void storeInVBO(Mesh & mesh);
	static void storeInVAO(Mesh & mesh);

	void setRenderMode(const RenderMode m);
	void switchRenderMode();
	void update() override;
};

#endif