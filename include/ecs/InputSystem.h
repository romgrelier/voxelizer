#ifndef INPUTSYSTEM__H
#define INPUTSYSTEM__H

#include "System.h"

class InputSystem : public System{
    private:
        sf::Window * m_window;
        unsigned int m_id_selection;
    public:
        InputSystem(EntityFactory & entityFactory, Engine & engine, sf::Window * w);
        ~InputSystem(){};

        void update() override;
};

#endif