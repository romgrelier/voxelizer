#ifndef NAVIGATION_SYSTEM__H
#define NAVIGATION_SYSTEM__H

#include "System.h"

#include <cmath>

class NavigationSystem : public System{
    private:
		bool isCollide(const CollisionBox & tBox1, const CollisionBox & tBox2) const;
		void resolvePath(Mouvement & mouv);

    public:
	    NavigationSystem(EntityFactory & entityFactory, Engine & engine);
	    ~NavigationSystem();

	    void update() override;
};

#endif