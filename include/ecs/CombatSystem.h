#ifndef COMBAT_SYSTEM__H
#define COMBAT_SYSTEM__H

#include "System.h"

class CombatSystem : public System{
private:

public:
	CombatSystem(EntityFactory & entityFactory, Engine & engine);
	~CombatSystem(){};
	void update() override;
};

#endif