#ifndef COMPONENT__H
#define COMPONENT__H

#include "../global.h"
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <glm/glm.hpp>
#include <array>
#include <vector>
#include <queue>
//#include <ctime>
#include <chrono>

enum e_Component{
	cmp_Mouvement = 0,
	cmp_Mesh,
	cmp_CollisionBox,
	cmp_Chunk,
	cmp_Block,
	cmp_UnitChunk,
	cmp_healthBar,
	cmp_weapon
};

enum BlockType{
    air = 0,
    grass,
    dirt,
    stone,
    sand,
    wood,
    water,
};

class Message{
public:
	bool updated{false};
};

typedef struct{
    GLdouble x;
    GLdouble y;
    GLdouble z;
}myVec3;

class RenderSystemMessage : public Message{
public:
	bool switchRenderMode;
	bool isContext;
	myVec3 m_start, m_end;
};

class NavigationSystemMessage : public Message{
public:
	bool updateAll;
};

/* ================================================================================= */

class Component{
};

struct PathMap{
	std::array<std::array<int, CHUNK_SIZE_X>, CHUNK_SIZE_Z> m_map;
};

struct Player{
	unsigned int score{0};
};

struct HealthBar : public Component{
public:
	HealthBar(const unsigned int health) : m_health{health}{}
	unsigned int m_health;
};

struct Weapon : public Component{
	Weapon(const unsigned int damage) : m_damage{damage}{}
	unsigned int m_damage;
	unsigned int m_targetId;
	bool m_isFiring;
};

struct Camera : public Component{
	public:
	Camera(glm::vec3 position, glm::vec3 target, glm::vec3 vectorV) : 
		m_position{position},
		m_target{target},
		m_vectorV{vectorV}{
	}
	glm::vec3 m_position;
	glm::vec3 m_target;
	glm::vec3 m_vectorV;
};

struct Mouvement : public Component{
public:
	glm::vec3 m_position;
	glm::vec3 m_direction;
	glm::vec3 m_target;
	std::queue<glm::vec3> m_nextNode;
	Mouvement(){};
	Mouvement(const float p_x, const float p_y, const float p_z) : 
		m_position{p_x, p_y, p_z}{
	}
	Mouvement(const float p_x, const float p_y, const float p_z, const float d_x, const float d_y, const float d_z) : 
		m_position{p_x, p_y, p_z},
		m_direction{d_x, d_y, d_z}{
	}
};

struct Mesh : public Component{
public:/*
	~Mesh(){
		glDeleteLists(m_displayList, 1);
   	 	glDeleteBuffers(1, &m_vbo);
    	glDeleteVertexArrays(1, &m_vao);
	}*/

	std::vector<float> m_vertices;
	std::vector<float> m_normal;
	std::vector<float> m_color;

	GLuint m_displayList;
	GLuint m_vbo;
	GLuint m_vao;
};

struct CollisionBox : public Component{
public:
	CollisionBox(const CollisionBox & c) : m_vecMin{c.m_vecMin}, m_vecMax{c.m_vecMax}{}
	CollisionBox(const glm::vec3 vecMin, const glm::vec3 vecMax) : m_vecMin{vecMin}, m_vecMax{vecMax}{}
	glm::vec3 m_vecMin;
    glm::vec3 m_vecMax;
};

class Block : public Component
{
public:
	BlockType m_block;
    static glm::vec4 getColor(const BlockType b);
};

struct Chunk : public Component{
public:
	std::array<std::array<std::array<BlockType, CHUNK_SIZE_Z>, CHUNK_SIZE_Y>, CHUNK_SIZE_X> m_chunk;
	//std::vector<std::vector<std::vector<BlockType>>> * m_chunk;
/*
	Chunk(const unsigned int size_x, const unsigned int size_y, const unsigned int size_z) {
		m_chunk = new std::vector<std::vector<std::vector<BlockType>(size_x)>(size_y)>(size_z);
	}
*/
	void setBlock(const BlockType type, const unsigned int x, const unsigned int y, const unsigned int z){
    	m_chunk[x][y][z] = type;
	}

	BlockType getBlock(const unsigned int x, const unsigned int y, const unsigned int z) const{
	    return m_chunk[x][y][z];
	}
};

struct UnitChunk: public Component{
public:
	std::array<std::array<std::array<BlockType, 10>, 10>, 10> m_chunk;

	void setBlock(const BlockType type, const unsigned int x, const unsigned int y, const unsigned int z){
    	m_chunk[x][y][z] = type;
	}

	BlockType getBlock(const unsigned int x, const unsigned int y, const unsigned int z) const{
	    return m_chunk[x][y][z];
	}
};

#endif
