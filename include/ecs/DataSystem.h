#ifndef DATA_SYSTEM__H
#define DATA_SYSTEM__H

#include "System.h"

class DataSystem : public System{
public:
	DataSystem(EntityFactory & entityFactory, Engine & engine);
	~DataSystem(){};
	void update() override;
};

#endif