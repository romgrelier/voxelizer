#ifndef ENTITYFACTORY__H
#define ENTITYFACTORY__H

#include "Entity.h"
#include "../WorldGenerator.h"
#include "../VoxelMesher.h"
#include "../ModelLoader.h"

//#include <sqlite3.h> 
#include <limits>
#include <algorithm>

#ifndef BUFFER_OFFSET
    #define BUFFER_OFFSET(offset) ((char*)NULL + (offset))
#endif

class EntityFactory{

private:
	std::vector<Entity> m_entity;

	std::vector<Mouvement> c_mouvement;
	std::vector<Mesh> c_mesh;
	std::vector<CollisionBox> c_collisionBox;
	std::vector<HealthBar> c_healthBar;
	std::vector<Weapon> c_weapon;

	std::vector<Chunk*> c_chunk;
	std::vector<UnitChunk*> c_unitChunk;

	Camera m_camera;

	PathMap * m_pathMap;

public:
	EntityFactory();
	~EntityFactory();

	Entity & createEntity();
	Entity & createUnit(const float x, const float y, const float z);
	Entity & createWorld();
	Entity & createCamera();
	Entity & createTarget();
	Entity & createControlPoint(const float x, const float y, const float z);

	std::vector<Mouvement> & getMouvement();
	std::vector<Mesh> & getMesh();
	std::vector<CollisionBox> & getCollision();
	std::vector<Weapon> & getWeapon();
	std::vector<HealthBar> & getHealthBar();
	
	std::vector<Entity> & getEntities();

	Camera & getCamera();
	PathMap * getPathMap();

	//void testDatabase();
};

#endif
