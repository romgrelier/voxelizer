#ifndef UTIL_H
#define UTIL_H

#include <glm/glm.hpp>

class Util
{
public:
    Util();
    static glm::vec3 calcul_normale(
            const float s0x, const float s0y, const float s0z,
            const float s1x, const float s1y, const float s1z,
            const float s2x, const float s2y, const float s2z);

    static glm::vec3 calcul_normale_sommet(
            const float v0x, const float v0y, const float v0z,
            const float v1x, const float v1y, const float v1z,
            const float v2x, const float v2y, const float v2z);
};

#endif // UTIL_H
