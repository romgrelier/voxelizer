#ifndef BLOCK_H
#define BLOCK_H

#include <glm/glm.hpp>

enum BlockType{
    air = 0,
    grass,
    dirt,
    stone,
    sand,
    wood,
    water,
};

class Block
{
public:
    static glm::vec4 getColor(const BlockType b);
};

#endif // BLOCK_H
