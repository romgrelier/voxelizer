#include "../include/Engine.h"

Engine::Engine(EntityFactory & factory) : m_entityFactory{factory}{
    sf::ContextSettings settings;
    settings.depthBits = 24;
    settings.stencilBits = 8;
    settings.antialiasingLevel = 0;
    settings.majorVersion = 2;
    settings.minorVersion = 1;

    m_window = new sf::Window(sf::VideoMode(640, 480), "TryEngine", sf::Style::Default, settings);

    m_window->setVerticalSyncEnabled(true);
    m_window->setActive(true);

    glewInit();

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHT0);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

Engine::~Engine(){
    for(auto & s : m_system){
        delete s.second;
    }
    delete m_window;
}

void Engine::addSystem(e_system s){
    switch(s){
        case sys_input:
            m_system.insert(std::pair<e_system, System*>(sys_input, new InputSystem(m_entityFactory, *this, m_window)));
        break;
        case sys_render:
            m_system.insert(std::pair<e_system, System*>(sys_render, new RenderSystem(m_entityFactory, *this, m_window)));
        break;
        case sys_navigation:
            m_system.insert(std::pair<e_system, System*>(sys_navigation, new NavigationSystem(m_entityFactory, *this)));
        break;
        case sys_combat:
            m_system.insert(std::pair<e_system, System*>(sys_combat, new CombatSystem(m_entityFactory, *this)));
        break;
        case sys_data:
            m_system.insert(std::pair<e_system, System*>(sys_data, new DataSystem(m_entityFactory, *this)));
        break;
        case sys_collisionBox:
        break;
    }
}

System * Engine::getSystem(const e_system s) { 
    return m_system[s]; 
}

void Engine::update(){
//*/
    for(auto & s : m_system){
            s.second->update();
    }
    
/*/
    std::vector<std::thread> threads;

    for(auto & s : m_system){
            threads.push_back(std::thread(&System::update, s.second));
    }

    for(auto & t : threads){
            t.join();
    }
/**/
}

void Engine::run(){
    while(m_window->isOpen()){
        update();
        m_window->display();
    }
}

sf::Window * Engine::getContext() { 
    return m_window; 
}