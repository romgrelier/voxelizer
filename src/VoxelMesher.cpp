#include "../include/VoxelMesher.h"
#include <iostream>

const float VoxelMesher::size = 0.5f;

void VoxelMesher::generate(Chunk * chunk, Mesh & mesh){
    unsigned int blockCount{0}, faceCount{0};

    for(unsigned int x = 0; x < CHUNK_SIZE_X; x++) {
        for(unsigned int y = 0; y < CHUNK_SIZE_Y; y++) {
            for(unsigned int z = 0; z < CHUNK_SIZE_Z; z++) {
                if(chunk->getBlock(x, y, z) != BlockType::air) {
                    blockCount++;

                    if(x == 0){ 
                        pushNX(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                        faceCount++;
                    }
                    if(x == CHUNK_SIZE_X - 1) { 
                        pushPX(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                        faceCount++;
                        }
                    if(x > 0){
                        if(chunk->getBlock(x-1, y, z) == BlockType::air) {
                            pushNX(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                            faceCount++;
                        }
                    }
                    if(x < CHUNK_SIZE_X - 1){
                        if(chunk->getBlock(x+1, y, z) == BlockType::air) {
                            pushPX(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                            faceCount++;
                        }
                    }

                    if(y == 0) {
                        pushNY(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                        faceCount++;
                    }
                    if(y == CHUNK_SIZE_Y - 1) {
                        pushPY(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                        faceCount++;
                    }
                    if(y > 0) {
                        if(chunk->getBlock(x, y-1, z) == BlockType::air){
                            pushNY(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                            faceCount++;
                        }
                    }
                    if(y < CHUNK_SIZE_Y - 1) {
                        if(chunk->getBlock(x, y+1, z) == BlockType::air){
                            pushPY(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                            faceCount++;
                        }
                    }

                    if(z == 0){ 
                        pushNZ(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                        faceCount++;
                    }
                    if(z == CHUNK_SIZE_Z - 1) {
                        pushPZ(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                        faceCount++;
                    }
                    if(z > 0){
                        if(chunk->getBlock(x, y, z-1) == BlockType::air){
                            pushNZ(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                            faceCount++;
                        }
                    }
                    if(z < CHUNK_SIZE_Z - 1){
                        if(chunk->getBlock(x, y, z+1) == BlockType::air){
                            pushPZ(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                            faceCount++;
                        }
                    }
                }
            }
        }
    }
    computeNormals(mesh);

    std::cout << "Nombre de Voxel = " << blockCount << std::endl;
    std::cout << "Nombre de faces = " << faceCount << std::endl;
}

void VoxelMesher::generateUnit(UnitChunk * chunk, Mesh & mesh){
    for(unsigned int x = 0; x < 10; x++){
        for(unsigned int y = 0; y < 10; y++){
            for(unsigned int z = 0; z < 10; z++){
                if(chunk->getBlock(x, y, z) != BlockType::air){
                    if(x == 0) pushNX(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                    if(x == 9) pushPX(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                    if(x > 0){
                        if(chunk->getBlock(x-1, y, z) == BlockType::air)
                            pushNX(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                    }
                    if(x < 9){
                        if(chunk->getBlock(x+1, y, z) == BlockType::air)
                            pushPX(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                    }
                    if(y == 0) pushNY(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                    if(y == 9) pushPY(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                    if(y > 0){
                        if(chunk->getBlock(x, y-1, z) == BlockType::air)
                            pushNY(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                    }
                    if(y < 9){
                        if(chunk->getBlock(x, y+1, z) == BlockType::air)
                            pushPY(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                    }

                    if(z == 0) pushNZ(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                    if(z == 9) pushPZ(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                    if(z > 0){
                        if(chunk->getBlock(x, y, z-1) == BlockType::air)
                            pushNZ(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                    }
                    if(z < 9){
                        if(chunk->getBlock(x, y, z+1) == BlockType::air)
                            pushPZ(x, y, z, Block::getColor(chunk->getBlock(x, y, z)), mesh);
                    }
                }
            }
        }
    }
    computeNormals(mesh);
}

void VoxelMesher::pushPX(const unsigned int x, const unsigned int y, const unsigned int z, const glm::vec4 color, Mesh & mesh){
    auto & m_color = mesh.m_color;
    auto & m_vertices = mesh.m_vertices;
    auto & m_normal = mesh.m_normal;

    float vertices[18] = {
        x + size, y + size, z + size,
        x + size, y - size, z + size,
        x + size, y - size, z - size,
        x + size, y - size, z - size,
        x + size, y + size, z - size,
        x + size, y + size, z + size
    };

    float colors[24] = {
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
    };


    for(unsigned int i = 0; i < 24; i++){
        m_color.push_back(colors[i]);
    }

    for(unsigned int i = 0; i < 18; i++){
        m_vertices.push_back(vertices[i]);
    }
}

void VoxelMesher::pushNX(const unsigned int x, const unsigned int y, const unsigned int z, const glm::vec4 color, Mesh & mesh){
    auto & m_color = mesh.m_color;
    auto & m_vertices = mesh.m_vertices;
    auto & m_normal = mesh.m_normal;
    
    float vertices[18] = {
        x - size, y - size, z + size,
        x - size, y + size, z + size,
        x - size, y - size, z - size,
        x - size, y + size, z - size,
        x - size, y - size, z - size,
        x - size, y + size, z + size
    };

    for(unsigned int i = 0; i < 18; i++){
        m_vertices.push_back(vertices[i]);
    }

    float colors[24] = {
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
    };

    for(unsigned int i = 0; i < 24; i++){
        m_color.push_back(colors[i]);
    }
}


void VoxelMesher::pushPY(const unsigned int x, const unsigned int y, const unsigned int z, const glm::vec4 color, Mesh & mesh){
    auto & m_color = mesh.m_color;
    auto & m_vertices = mesh.m_vertices;
    auto & m_normal = mesh.m_normal;

    float vertices[18] = {
        x - size, y + size, z + size,
        x + size, y + size, z + size,
        x + size, y + size, z - size,
        x + size, y + size, z - size,
        x - size, y + size, z - size,
        x - size, y + size, z + size
    };

    for(unsigned int i = 0; i < 18; i++){
        m_vertices.push_back(vertices[i]);
    }

    float colors[24] = {
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
    };

    for(unsigned int i = 0; i < 24; i++){
        m_color.push_back(colors[i]);
    }
}

void VoxelMesher::pushNY(const unsigned int x, const unsigned int y, const unsigned int z, const glm::vec4 color, Mesh & mesh){
    auto & m_color = mesh.m_color;
    auto & m_vertices = mesh.m_vertices;
    auto & m_normal = mesh.m_normal;

    float vertices[18] = {
        x + size, y - size, z + size,
        x - size, y - size, z + size,
        x + size, y - size, z - size,
        x - size, y - size, z - size,
        x + size, y - size, z - size,
        x - size, y - size, z + size
    };

    for(unsigned int i = 0; i < 18; i++){
        m_vertices.push_back(vertices[i]);
    }

    float colors[24] = {
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
    };

    for(unsigned int i = 0; i < 24; i++){
        m_color.push_back(colors[i]);
    }
}


void VoxelMesher::pushPZ(const unsigned int x, const unsigned int y, const unsigned int z, const glm::vec4 color, Mesh & mesh){
    auto & m_color = mesh.m_color;
    auto & m_vertices = mesh.m_vertices;
    auto & m_normal = mesh.m_normal;

    float vertices[18] = {
        x - size, y + size, z + size,
        x - size, y - size, z + size,
        x + size, y - size, z + size,
        x + size, y - size, z + size,
        x + size, y + size, z + size,
        x - size, y + size, z + size
    };

    for(unsigned int i = 0; i < 18; i++){
        m_vertices.push_back(vertices[i]);
    }

    float colors[24] = {
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
    };

    for(unsigned int i = 0; i < 24; i++){
        m_color.push_back(colors[i]);
    }

}

void VoxelMesher::pushNZ(const unsigned int x, const unsigned int y, const unsigned int z, const glm::vec4 color, Mesh & mesh){
    auto & m_color = mesh.m_color;
    auto & m_vertices = mesh.m_vertices;
    auto & m_normal = mesh.m_normal;
    
    float vertices[18] = {
        x - size, y - size, z - size,
        x - size, y + size, z - size,
        x + size, y - size, z - size,
        x + size, y + size, z - size,
        x + size, y - size, z - size,
        x - size, y + size, z - size
    };

    for(unsigned int i = 0; i < 18; i++){
        m_vertices.push_back(vertices[i]);
    }

    float colors[24] = {
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
        color.r, color.g, color.b, color.a,
    };

    for(unsigned int i = 0; i < 24; i++){
        m_color.push_back(colors[i]);
    }
}

void VoxelMesher::computeNormals(Mesh & mesh){
    auto & m_color = mesh.m_color;
    auto & m_vertices = mesh.m_vertices;
    auto & m_normal = mesh.m_normal;

    for(unsigned int i = 0; i < m_vertices.size(); i+=9){
        glm::vec3 vec = Util::calcul_normale(
                    m_vertices.at(i), m_vertices.at(i+1), m_vertices.at(i+2),
                    m_vertices.at(i+3), m_vertices.at(i+4), m_vertices.at(i+5),
                    m_vertices.at(i+6), m_vertices.at(i+7), m_vertices.at(i+8)
                    );

        m_normal.push_back(vec.x);
        m_normal.push_back(vec.y);
        m_normal.push_back(vec.z);

        m_normal.push_back(vec.x);
        m_normal.push_back(vec.y);
        m_normal.push_back(vec.z);

        m_normal.push_back(vec.x);
        m_normal.push_back(vec.y);
        m_normal.push_back(vec.z);
    }
}