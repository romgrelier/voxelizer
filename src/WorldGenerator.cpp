#include "../include/WorldGenerator.h"

using namespace noise;

void WorldGenerator::generateChunk(Chunk * chunk){
    std::cout << "génération de la carte" << std::endl;
    module::Perlin myModule;
    myModule.SetOctaveCount(8);
    myModule.SetFrequency(2);

    utils::NoiseMap * heightMap = new utils::NoiseMap;
    noise::utils::NoiseMapBuilderPlane heightMapBuilder;
    heightMapBuilder.SetSourceModule(myModule);
    heightMapBuilder.SetDestNoiseMap(*heightMap);
    heightMapBuilder.SetDestSize(CHUNK_SIZE_X, CHUNK_SIZE_Z);
    heightMapBuilder.SetBounds (8.0, 10.0, 8.0, 10.0); // -x | +x | -z | +z
    heightMapBuilder.Build();

    utils::RendererImage renderer;
    utils::Image image;
    renderer.SetSourceNoiseMap(*heightMap);
    renderer.SetDestImage(image);
    renderer.ClearGradient();
    renderer.AddGradientPoint (-1.0000, utils::Color (  0,   0, 128, 255)); // deeps
    renderer.AddGradientPoint (-0.2500, utils::Color (  0,   0, 255, 255)); // shallow
    renderer.AddGradientPoint ( 0.0000, utils::Color (  0, 128, 255, 255)); // shore
    renderer.AddGradientPoint ( 0.0625, utils::Color (240, 240,  64, 255)); // sand
    renderer.AddGradientPoint ( 0.1250, utils::Color ( 32, 160,   0, 255)); // grass
    renderer.AddGradientPoint ( 0.3750, utils::Color (224, 224,   0, 255)); // dirt
    renderer.AddGradientPoint ( 0.7500, utils::Color (128, 128, 128, 255)); // rock
    renderer.AddGradientPoint ( 1.0000, utils::Color (255, 255, 255, 255)); // snow
    renderer.Render();

    utils::WriterBMP writer;
    writer.SetSourceImage (image);
    writer.SetDestFilename ("heightmap.bmp");
    writer.WriteDestFile();

    std::cout << "remplissage du chunk" << std::endl;
    for(unsigned int x = 0; x < CHUNK_SIZE_X; x++){
        for(unsigned int z = 0; z < CHUNK_SIZE_Z; z++){
            //unsigned int y = (unsigned int) ((heightMap->GetValue(x, z) + 1) * (CHUNK_SIZE_Y - 1)) / 2;
            unsigned int y = pow(heightMap->GetValue(x, z), 2.0) * (double)(CHUNK_SIZE_Y - 1);

            /*if(y < 0 || y > CHUNK_SIZE_Y - 1){
                std::cout << y << "-";
            }*/

            if(y >= CHUNK_SIZE_Y) y = 0;
            if(y > CHUNK_SIZE_Y) y = CHUNK_SIZE_Y;

            if(heightMap->GetValue(x, z) >= -1 && heightMap->GetValue(x, z) < -0.25){
                chunk->setBlock(BlockType::water, x, y, z);
            }else if(heightMap->GetValue(x, z) >= -0.25 && heightMap->GetValue(x, z) < 0.0){
                chunk->setBlock(BlockType::sand, x, y, z);
            }else if(heightMap->GetValue(x, z) >= 0.0 && heightMap->GetValue(x, z) < 0.0625){
                chunk->setBlock(BlockType::sand, x, y, z);
            }else if(heightMap->GetValue(x, z) >= 0.0625 && heightMap->GetValue(x, z) < 0.125){
                chunk->setBlock(BlockType::dirt, x, y, z);
            }else if(heightMap->GetValue(x, z) >= 0.125 && heightMap->GetValue(x, z) < 0.375){
                chunk->setBlock(BlockType::grass, x, y, z);
            }else if(heightMap->GetValue(x, z) >= 0.375 && heightMap->GetValue(x, z) < 0.75){
                chunk->setBlock(BlockType::wood, x, y, z);
            }else if(heightMap->GetValue(x, z) >= 0.375 && heightMap->GetValue(x, z) < 0.75){
                chunk->setBlock(BlockType::grass, x, y, z);
            }else{
                chunk->setBlock(BlockType::water, x, y, z);
            }

            // sous la heightmap
            int i = y - 1;
            while(i >= 0){
                chunk->setBlock(BlockType::stone, x, i--, z);
            }

        }
    }

    delete heightMap;
}

void WorldGenerator::generatePathMap(const Chunk * chunk, PathMap * pathMap){
    for(unsigned int x{0}; x < CHUNK_SIZE_X; x++){
        for(unsigned int z{0}; z < CHUNK_SIZE_Z; z++){

            unsigned int y{CHUNK_SIZE_Y - 1}; // on part du haut du chunk
            while(chunk->getBlock(x, y, z) == BlockType::air){ // on attend le premier block du terrain
                y--;
            }
            pathMap->m_map[x][z] = y; // on attribut la hauteur comme coût

        }
    }

    for(unsigned int x = 0; x < CHUNK_SIZE_X; x++){
        for(unsigned int z = 0; z < CHUNK_SIZE_Z; z++){
            std::cout << pathMap->m_map[x][z] << " ";
        }
        std::cout << std::endl;
    }
}