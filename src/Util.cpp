#include "../include/Util.h"

Util::Util()
{

}

glm::vec3 Util::calcul_normale(
    const float s0x, const float s0y, const float s0z,
    const float s1x, const float s1y, const float s1z,
    const float s2x, const float s2y, const float s2z) {

    float nx, ny, nz;

    float v0x = s2x - s1x;
    float v0y = s2y - s1y;
    float v0z = s2z - s1z;
    float v1x = s0x - s1x;
    float v1y = s0y - s1y;
    float v1z = s0z - s1z;

    nx = (v0y * v1z) - (v0z *v1y);
    ny = (v0z * v1x) - (v0x *v1z);
    nz = (v0x * v1y) - (v0y *v1x);

    float norme = sqrtf(nx * nx + ny * ny + nz * nz);

    return glm::vec3(
        nx / norme,
        ny / norme,
        nz / norme
        );

}

glm::vec3 Util::calcul_normale_sommet(
        const float v0x, const float v0y, const float v0z,
        const float v1x, const float v1y, const float v1z,
        const float v2x, const float v2y, const float v2z){

        float Nx = v0x + v1x + v2x;
        float Ny = v0y + v1y + v2y;
        float Nz = v0z + v1z + v2z;

        float N_abs = sqrt(pow(Nx, 2) + pow(Ny, 2) + pow(Nz, 2));

        return glm::vec3(Nx/N_abs, Ny/N_abs, Nz/N_abs);
}
