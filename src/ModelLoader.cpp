#include "../include/ModelLoader.h"

void ModelLoader::loadTank(UnitChunk * chunk){
	for(unsigned int i{0}; i < 5; i++){
		chunk->setBlock(BlockType::stone, 0, i, 0);
		chunk->setBlock(BlockType::stone, 0, i, 1);
		chunk->setBlock(BlockType::stone, 1, i, 1);
		chunk->setBlock(BlockType::stone, 1, i, 0);
	}
}