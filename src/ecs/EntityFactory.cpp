#include "../../include/ecs/EntityFactory.h"

EntityFactory::EntityFactory() : 
	m_camera{glm::vec3(20, CHUNK_SIZE_Y + 20, 20), glm::vec3(20, 0, 20 + CHUNK_SIZE_Y), glm::vec3(0, 1, 0)},
	m_pathMap{new PathMap}{
}

EntityFactory::~EntityFactory(){
	for(Chunk * c : c_chunk) delete c;
	for(UnitChunk * u : c_unitChunk) delete u;
	delete m_pathMap;
}

Entity & EntityFactory::createEntity(){
	m_entity.push_back(Entity());
	Entity & e = m_entity.back();
	/* exmaple */
	c_mouvement.push_back(Mouvement(e.getId(), e.getId(), e.getId()));
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_Mouvement, 0));
	c_mesh.push_back(Mesh());
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_Mesh, 1));
	c_collisionBox.push_back(CollisionBox(glm::vec3(0, 0, 0), glm::vec3(1, 1, 1)));
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_CollisionBox, 2));
	/* example */
	return m_entity.back();
}

Entity & EntityFactory::createUnit(const float x, const float y, const float z){
	m_entity.push_back(Entity());
	Entity & e = m_entity.back();

	c_mouvement.push_back(Mouvement(x, y, z, 0, 0, 0));
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_Mouvement, c_mouvement.size() - 1));

	c_healthBar.push_back(HealthBar(100));
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_healthBar, c_healthBar.size() - 1));

	c_weapon.push_back(Weapon(10));
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_weapon, c_weapon.size() - 1));

	c_collisionBox.push_back(CollisionBox(glm::vec3(x - 1, y + 1, z - 1), glm::vec3(x + 1, y + 5, z + 1)));
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_CollisionBox, c_mouvement.size() - 1));

	c_unitChunk.push_back(new UnitChunk());
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_UnitChunk, c_unitChunk.size() - 1));
	ModelLoader::loadTank(c_unitChunk.back());

	c_mesh.push_back(Mesh());
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_Mesh, c_mesh.size() - 1));
	Mesh & mesh{c_mesh.back()};
	VoxelMesher::generateUnit(c_unitChunk.back(), c_mesh.back());
	
	// DATA MESH/
	 // VBO
		glGenBuffers(1, &mesh.m_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, mesh.m_vbo);

		glBufferData(GL_ARRAY_BUFFER,
					sizeof(GLfloat) * mesh.m_vertices.size()
					+ sizeof(GLfloat) * mesh.m_color.size()
					+ sizeof(GLfloat) * mesh.m_normal.size(), 
					nullptr,
					GL_STATIC_DRAW);

		glBufferSubData(GL_ARRAY_BUFFER,
						0,
						sizeof(GLfloat) * mesh.m_vertices.size(),
						&mesh.m_vertices.front());

		glBufferSubData(GL_ARRAY_BUFFER,
						sizeof(GLfloat) * mesh.m_vertices.size(),
						sizeof(GLfloat) * mesh.m_color.size(),
						&mesh.m_color.front());

		glBufferSubData(GL_ARRAY_BUFFER,
						sizeof(GLfloat) * mesh.m_vertices.size()
						+ sizeof(GLfloat) * mesh.m_color.size(),
						sizeof(GLfloat) * mesh.m_normal.size(),
						&mesh.m_normal.front());

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	 // VAO
		glGenVertexArrays(1, &mesh.m_vao);
		glBindVertexArray(mesh.m_vao);

		glBindBuffer(GL_ARRAY_BUFFER, mesh.m_vbo);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(GLfloat)*mesh.m_vertices.size()));
		glEnableVertexAttribArray(3);

		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(GLfloat)*mesh.m_vertices.size() + sizeof(GLfloat)*mesh.m_color.size()));
		glEnableVertexAttribArray(2);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);
	// \DATA MESH
	
	return m_entity.back();
}

Entity & EntityFactory::createWorld(){
	m_entity.push_back(Entity());
	Entity & e = m_entity.back();

	//create a chunk with Perlin's noise
	std::cout << "génération du chunk" << std::endl;
	c_chunk.push_back(new Chunk());
	WorldGenerator::generateChunk(c_chunk.back());
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_Chunk, c_chunk.size() - 1));
	std::cout << "génération de la pathMap" << std::endl;
	WorldGenerator::generatePathMap(c_chunk.back(), m_pathMap);

	c_mouvement.push_back(Mouvement(0, 0, 0, 0, 0, 0));
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_Mouvement, c_mouvement.size() - 1));
	c_collisionBox.push_back(CollisionBox(glm::vec3(1, 0, -1), glm::vec3(-1, 0, 1)));
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_CollisionBox, c_mouvement.size() - 1));

	// create its mesh
	std::cout << "génération du mesh" << std::endl;
	c_mesh.push_back(Mesh());
	Mesh & mesh{c_mesh.back()};
	VoxelMesher::generate(c_chunk.back(), mesh);
	// Display List
		mesh.m_displayList = glGenLists(1);
		glNewList(mesh.m_displayList, GL_COMPILE);
		unsigned int j{0};

		glBegin(GL_TRIANGLES);
		for(unsigned int i{0}; i < mesh.m_vertices.size(); i+=3){
			glNormal3f(mesh.m_normal[i], mesh.m_normal[i+1], mesh.m_normal[i+2]);
			glColor4f(mesh.m_color[j], mesh.m_color[j+1], mesh.m_color[j+2], mesh.m_color[j+3]);
			j+=4;
			glVertex3f(mesh.m_vertices[i], mesh.m_vertices[i+1], mesh.m_vertices[i+2]);
		}
		glEnd();
		glEndList();
	 // VBO
		glGenBuffers(1, &mesh.m_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, mesh.m_vbo);

		glBufferData(GL_ARRAY_BUFFER,
					sizeof(GLfloat) * mesh.m_vertices.size()
					+ sizeof(GLfloat) * mesh.m_color.size()
					+ sizeof(GLfloat) * mesh.m_normal.size(),
					nullptr,
					GL_STATIC_DRAW);

		glBufferSubData(GL_ARRAY_BUFFER,
						0,
						sizeof(GLfloat) * mesh.m_vertices.size(),
						&mesh.m_vertices.front());

		glBufferSubData(GL_ARRAY_BUFFER,
						sizeof(GLfloat) * mesh.m_vertices.size(),
						sizeof(GLfloat) * mesh.m_color.size(),
						&mesh.m_color.front());

		glBufferSubData(GL_ARRAY_BUFFER,
						sizeof(GLfloat) * mesh.m_vertices.size()
						+ sizeof(GLfloat) * mesh.m_color.size(),
						sizeof(GLfloat) * mesh.m_normal.size(),
						&mesh.m_normal.front());

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	 // VAO
		glGenVertexArrays(1, &mesh.m_vao);
		glBindVertexArray(mesh.m_vao);

		glBindBuffer(GL_ARRAY_BUFFER, mesh.m_vbo);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(GLfloat)*mesh.m_vertices.size()));
		glEnableVertexAttribArray(3);

		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(GLfloat)*mesh.m_vertices.size() + sizeof(GLfloat)*mesh.m_color.size()));
		glEnableVertexAttribArray(2);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);
	
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_Mesh, c_mesh.size() - 1));

	std::cout << "vertices = " << c_mesh.back().m_vertices.size() << std::endl;
	std::cout << "normals = " << c_mesh.back().m_normal.size() << std::endl;
	std::cout << "colors = " << c_mesh.back().m_color.size() << std::endl;

	std::cout << "terminé" << std::endl;

	return m_entity.back();
}

Entity & EntityFactory::createCamera(){
	m_entity.push_back(Entity());
	Entity & e = m_entity.back();
	
	return m_entity.back();
}

Entity & EntityFactory::createTarget(){
	m_entity.push_back(Entity());
	Entity & e = m_entity.back();
	
	c_mouvement.push_back(Mouvement(64, 64, 32));
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_Mouvement, c_mouvement.size() - 1));

	return m_entity.back();
}

Entity & EntityFactory::createControlPoint(const float x, const float y, const float z){
	m_entity.push_back(Entity());
	Entity & e = m_entity.back();

	c_mouvement.push_back(Mouvement(x, y, z, 0, 0, 0));
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_Mouvement, c_mouvement.size() - 1));

	c_collisionBox.push_back(CollisionBox(glm::vec3(-4, 1, -4), glm::vec3(4, 9, 4)));
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_CollisionBox, c_mouvement.size() - 1));

	c_unitChunk.push_back(new UnitChunk());
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_UnitChunk, c_unitChunk.size() - 1));
	ModelLoader::loadTank(c_unitChunk.back());

	c_mesh.push_back(Mesh());
	e.m_components.insert(std::pair<e_Component, unsigned int>(cmp_Mesh, c_mesh.size() - 1));
	Mesh & mesh{c_mesh.back()};
	VoxelMesher::generateUnit(c_unitChunk.back(), c_mesh.back());

	return e;
}

std::vector<Mouvement> & EntityFactory::getMouvement(){ 
	return c_mouvement; 
}

std::vector<Mesh> & EntityFactory::getMesh(){ 
	return c_mesh; 
}

std::vector<CollisionBox> & EntityFactory::getCollision(){ 
	return c_collisionBox; 
}

std::vector<Weapon> & EntityFactory::getWeapon(){
	return c_weapon;
}

std::vector<HealthBar> & EntityFactory::getHealthBar(){
	return c_healthBar;
}

std::vector<Entity> & EntityFactory::getEntities(){ 
	return m_entity; 
}
 
Camera & EntityFactory::getCamera() { 
	return m_camera; 
}

PathMap * EntityFactory::getPathMap(){
	return m_pathMap;
}

static int callback(void *NotUsed, int argc, char **argv, char **azColName){
   int i;
   for(i=0; i<argc; i++){
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   return 0;
}
/*
void EntityFactory::testDatabase(){
	sqlite3 *db;
	char *zErrMsg = 0;
	int rc;
	char * req = "SELECT * FROM Component";

	rc = sqlite3_open("test.db", &db);

	if( rc ){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	}else{
		fprintf(stderr, "Opened database successfully\n");

		rc = sqlite3_exec(db, req, callback, 0, &zErrMsg);
		if( rc != SQLITE_OK ){
		  fprintf(stderr, "SQL error: %s\n", zErrMsg);
		  sqlite3_free(zErrMsg);
		}else{
		  fprintf(stdout, "Records created successfully\n");
		}

	  	sqlite3_close(db);
	}
}*/
