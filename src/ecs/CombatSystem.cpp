#include "../../include/ecs/CombatSystem.h"

CombatSystem::CombatSystem(EntityFactory & entityFactory, Engine & engine) : System(entityFactory, engine){

}

void CombatSystem::update(){
	auto & weapon_data{r_entityFactory.getWeapon()};
	auto & health_data{r_entityFactory.getHealthBar()};

	for(Entity & e : r_entityFactory.getEntities()){
		if(weapon_data[e.getMap()[cmp_weapon]].m_isFiring){
			Entity & target = r_entityFactory.getEntities()[weapon_data[e.getMap()[cmp_weapon]].m_targetId];

			health_data[target.getMap()[cmp_healthBar]].m_health -= weapon_data[e.getMap()[cmp_weapon]].m_damage;

			weapon_data[e.getMap()[cmp_weapon]].m_isFiring = false;

			if(health_data[target.getMap()[cmp_healthBar]].m_health <= 0){
				r_entityFactory.getEntities().erase(
					r_entityFactory.getEntities().begin() + weapon_data[e.getMap()[cmp_weapon]].m_targetId
					);
					weapon_data[e.getMap()[cmp_weapon]].m_targetId = 0;
			}

		}
	}
}