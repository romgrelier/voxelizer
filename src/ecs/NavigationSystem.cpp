#include "../../include/ecs/NavigationSystem.h"

extern NavigationSystemMessage nsm;

NavigationSystem::NavigationSystem(EntityFactory & entityFactory, Engine & engine) : 
	System(entityFactory, engine){

}

NavigationSystem::~NavigationSystem(){

}

bool NavigationSystem::isCollide(const CollisionBox & tBox1, const CollisionBox & tBox2) const{
    return
        tBox1.m_vecMax.x > tBox2.m_vecMin.x &&
        tBox1.m_vecMin.x < tBox2.m_vecMax.x &&
        tBox1.m_vecMax.y > tBox2.m_vecMin.y &&
        tBox1.m_vecMin.y < tBox2.m_vecMax.y &&
        tBox1.m_vecMax.z > tBox2.m_vecMin.z &&
        tBox1.m_vecMin.z < tBox2.m_vecMax.z;
}

typedef struct {
	bool done{false};
	unsigned int cost{std::numeric_limits<unsigned int>::max()};
	glm::vec2 previous{glm::vec2(-1, -1)};
} Node;

/*
struct myVec2{
	myVec2(const unsigned int x, const unsigned int y) :
		x((unsigned int)x), y((unsigned int)y){};
	myVec2(const myVec2 vec) :
		vec.x((unsigned int)x), vec.y((unsigned int)y){};
	unsigned int x;
	unsigned int y;
};
typedef struct myVec2 myVec2;
*/
void NavigationSystem::resolvePath(Mouvement & mouv){
	PathMap * map{r_entityFactory.getPathMap()};

	glm::vec2 source(mouv.m_position.x, mouv.m_position.z); // noeud de départ
	glm::vec2 noeud(mouv.m_position.x, mouv.m_position.z); // noeud actuel
	glm::vec2 destination(mouv.m_target.x, mouv.m_target.z);

	// initilization
	std::array<std::array<Node, CHUNK_SIZE_X>, CHUNK_SIZE_Z> distances;
	// noeud source
	distances[noeud.x][noeud.y].cost = 0;
	// compteur de noeuds éffectués
	unsigned int count_done{0};

	// algorithme
	std::cout << "calcul itinéraire" << std::endl;
	while(count_done != CHUNK_SIZE_X * CHUNK_SIZE_Z){ // tant que tous les noeuds n'ont pas été visité
	//while(noeud != destination){ // tant que la destination n'a pas été atteinte
		std::cout << "count  = " << count_done << "/" << CHUNK_SIZE_X * CHUNK_SIZE_Z << std::endl;
		count_done++;
		distances[noeud.x][noeud.y].done = true;

		// renseigner les coûts en vérifiant les limites : coût du noeud actuel + la branche
		if(noeud.x + 1 < CHUNK_SIZE_X){
			if(distances[noeud.x+1][noeud.y].cost > distances[noeud.x][noeud.y].cost + std::abs(map->m_map[noeud.x+1][noeud.y] - map->m_map[noeud.x][noeud.y])){
				distances[noeud.x+1][noeud.y].cost = distances[noeud.x][noeud.y].cost + std::abs(map->m_map[noeud.x+1][noeud.y] - map->m_map[noeud.x][noeud.y]);
				distances[noeud.x+1][noeud.y].previous = noeud;
			}
		}
		else if(noeud.x - 1 >= 0){
			if(distances[noeud.x-1][noeud.y].cost > distances[noeud.x][noeud.y].cost + std::abs(map->m_map[noeud.x-1][noeud.y] - map->m_map[noeud.x][noeud.y])){
				distances[noeud.x-1][noeud.y].cost = distances[noeud.x][noeud.y].cost + std::abs(map->m_map[noeud.x-1][noeud.y] - map->m_map[noeud.x][noeud.y]);
				distances[noeud.x-1][noeud.y].previous = noeud;
			}
		}
		else if(noeud.y + 1 < CHUNK_SIZE_Y){
			if(distances[noeud.x][noeud.y+1].cost > distances[noeud.x][noeud.y].cost + std::abs(map->m_map[noeud.x][noeud.y+1] - map->m_map[noeud.x][noeud.y])){
				distances[noeud.x][noeud.y+1].cost = distances[noeud.x][noeud.y].cost + std::abs(map->m_map[noeud.x][noeud.y+1] - map->m_map[noeud.x][noeud.y]);
				distances[noeud.x][noeud.y+1].previous = noeud;
			}
		}
		else if(noeud.y - 1 >= 0){
			if(distances[noeud.x][noeud.y-1].cost > distances[noeud.x][noeud.y].cost + std::abs(map->m_map[noeud.x][noeud.y-1] - map->m_map[noeud.x][noeud.y])){
				distances[noeud.x][noeud.y-1].cost = distances[noeud.x][noeud.y].cost + std::abs(map->m_map[noeud.x][noeud.y-1] - map->m_map[noeud.x][noeud.y]);
				distances[noeud.x][noeud.y-1].previous = noeud;
			}
		}

		// trouver le moins chère parmis les noeuds renseignés
			// choix par défaut en vérifiant les limites de la grille
			glm::vec2 less(-1, -1);
			if(noeud.x + 1 < CHUNK_SIZE_X){
				less.x = noeud.x+1;
				less.y = noeud.y;
			}
			else if(noeud.x - 1 >= 0){
				less.x = noeud.x-1;
				less.y = noeud.y;
			}
			else if(noeud.y + 1 < CHUNK_SIZE_Y){
				less.x = noeud.x;
				less.y = noeud.y+1;
			}
			else if(noeud.y - 1 >= 0){
				less.x = noeud.x;
				less.y = noeud.y-1;
			}
			// vérification avec les autres noeuds et en restant dans la grille
			if(noeud.x + 1 < CHUNK_SIZE_X){
				if(distances[noeud.x+1][noeud.y].cost < distances[less.x][less.y].cost && distances[noeud.x+1][noeud.y].done == false){
					less.x = noeud.x+1;
				}
			}
			else if(noeud.x - 1 >= 0){
				if(distances[noeud.x-1][noeud.y].cost < distances[less.x][less.y].cost && distances[noeud.x-1][noeud.y].done == false){
					less.x = noeud.x-1;
				}
			}
			else if(noeud.y + 1 < CHUNK_SIZE_Y){
				if(distances[noeud.x][noeud.y+1].cost < distances[less.x][less.y].cost && distances[noeud.x][noeud.y+1].done == false){
					less.y = noeud.y+1;
				}
			}
			else if(noeud.y - 1 >= 0){
				if(distances[noeud.x][noeud.y-1].cost < distances[less.x][less.y].cost && distances[noeud.x][noeud.y-1].done == false){
					less.y = noeud.y-1;
				}
			}

		// passage au noeud le moins chère
		noeud.x = less.x;
		noeud.y = less.y;
	}

	// crétion du chemin
	std::queue<glm::vec3> & path{mouv.m_nextNode};
	noeud = distances[destination.x][destination.y].previous;
	std::cout << "création itinéraire" << std::endl;
	while(noeud != source){ // tant que l'on est pas arrivé
		path.push(glm::vec3(noeud.x, map->m_map[noeud.x][noeud.y], noeud.y));
		noeud = distances[noeud.x][noeud.y].previous;
	}
}

void NavigationSystem::update(){
	unsigned int count{0};
	auto & mouv_data = r_entityFactory.getMouvement();
	auto & collision_data = r_entityFactory.getCollision();

	//if(nsm.updateAll){

		for(Entity & e : r_entityFactory.getEntities()){
			glm::vec3 & position = mouv_data[e.getMap()[cmp_Mouvement]].m_position;
			glm::vec3 & direction = mouv_data[e.getMap()[cmp_Mouvement]].m_direction;
			glm::vec3 & target = mouv_data[e.getMap()[cmp_Mouvement]].m_target;
			std::queue<glm::vec3> & nextNode = mouv_data[e.getMap()[cmp_Mouvement]].m_nextNode;

			// pathfinding
			if(target != glm::vec3(0, 0, 0)){
				if(nextNode.size() == 0){ // calcul du passage
					//resolvePath(mouv_data[e.getMap()[cmp_Mouvement]]);
					/* Some test
					forœ i = 0; i < 10; i++){
						nextNode.push(glm::vec3(i, 0, i));
					}
					for(unsigned int i = 0; i < 10; i++){
						nextNode.push(glm::vec3(0, 0, i));
					}
					*/

					target.y = r_entityFactory.getPathMap()->m_map[target.x][target.z] - r_entityFactory.getPathMap()->m_map[position.x][position.z];

					nextNode.push(glm::vec3(
						target.x,
						target.y,
						target.z));

						std::cout << "target - x : " << target.x << " y : " << target.y << " z : " << target.z << std::endl;
				}

				// case suivante
				direction = nextNode.front();
				nextNode.pop();

				if(nextNode.size() == 0) target = glm::vec3(0, 0, 0); // si on a finit le déplacement on remet à zero

				// mouvement
				if(direction != glm::vec3(0, 0, 0)){
					glm::vec3 & position = mouv_data[e.getMap()[cmp_Mouvement]].m_position;

					// collisions
					CollisionBox e_box(collision_data[e.getMap()[cmp_CollisionBox]]);
					e_box.m_vecMax = glm::vec3(direction + e_box.m_vecMax);
					e_box.m_vecMin = glm::vec3(direction + e_box.m_vecMin);

					// collisions with entities
					for(Entity & target : r_entityFactory.getEntities()){
						if(target.getId() != e.getId()){
							const CollisionBox & t_box(collision_data[target.getMap()[cmp_CollisionBox]]);

							if(isCollide(e_box, t_box)){
								std::cout << "Collision detected : " << e.getId() << " with " << target.getId() << std::endl;
								count--;
							}
						}
						count++;
					}
					// \ collisions

					if(count == r_entityFactory.getEntities().size()){
						position += direction;
						CollisionBox & new_e_box(collision_data[e.getMap()[cmp_CollisionBox]]);
						new_e_box.m_vecMax += direction;
						new_e_box.m_vecMin += direction;
						std::cout << "position - x : " << position.x << " y : " << position.y << " z : " << position.z << std::endl;
					}

					count = 0;
					direction.x = 0;
					direction.y = 0;
					direction.z = 0;
				}
			}
		}

	//}

}