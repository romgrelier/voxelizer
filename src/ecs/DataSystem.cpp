#include "../../include/ecs/DataSystem.h"

DataSystem::DataSystem(EntityFactory & entityFactory, Engine & engine) :
	System(entityFactory, engine){

}

void DataSystem::update(){
	auto & weapon_data{r_entityFactory.getWeapon()};
	auto & health_data{r_entityFactory.getHealthBar()};

	for(Entity & e : r_entityFactory.getEntities()){
		std::cout << "id " << e.getId() << " | ";
		for(unsigned int i = 0; i < health_data[e.getMap()[cmp_weapon]].m_health; i++){
			std::cout << "|";
		}
		std::cout << std::endl;
	}
}