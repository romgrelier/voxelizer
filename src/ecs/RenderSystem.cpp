    #include "../../include/ecs/RenderSystem.h"

extern RenderSystemMessage rsm;

RenderSystem::RenderSystem(EntityFactory & entityFactory, Engine & engine, sf::Window * w) : 
    System(entityFactory, engine), 
    renderMode{RenderMode::vbo},
    m_context{w}{
        //w->setActive(true);
}

RenderSystem::~RenderSystem(){
    for(Mesh & mesh : r_entityFactory.getMesh()){
        glDeleteLists(mesh.m_displayList, 1);
        glDeleteBuffers(1, &mesh.m_vbo);
        glDeleteVertexArrays(1, &mesh.m_vao);
    }
}

void RenderSystem::render_immediate(const Mesh & mesh){
    unsigned int j{0};

    glBegin(GL_TRIANGLES);
    for(unsigned int i{0}; i < mesh.m_vertices.size(); i+=3){
        glNormal3f(mesh.m_normal[i], mesh.m_normal[i+1], mesh.m_normal[i+2]);
        glColor4f(mesh.m_color[j], mesh.m_color[j+1], mesh.m_color[j+2], mesh.m_color[j+3]);
        j+=4;
        glVertex3f(mesh.m_vertices[i], mesh.m_vertices[i+1], mesh.m_vertices[i+2]);
    }
    glEnd();
}

void RenderSystem::loadInDisplayList(Mesh & mesh){
    mesh.m_displayList = glGenLists(1);
    glNewList(mesh.m_displayList, GL_COMPILE);
    render_immediate(mesh);
    glEndList();
}

void RenderSystem::render_displayList(const Mesh & mesh) const{
    glCallList(mesh.m_displayList);
}

void RenderSystem::render_vertexArray(const Mesh & mesh) const{
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, &mesh.m_vertices.front());

    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(4, GL_FLOAT, 0, &mesh.m_color.front());

    glEnableClientState(GL_NORMAL_ARRAY);
    glNormalPointer(GL_FLOAT, 0, &mesh.m_normal.front());

    glDrawArrays(GL_TRIANGLES, 0, mesh.m_vertices.size() / 3);

    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
}

void RenderSystem::storeInVBO(Mesh & mesh){

}

void RenderSystem::render_vbo(const Mesh & mesh) const{
    glBindBuffer(GL_ARRAY_BUFFER, mesh.m_vbo);
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, BUFFER_OFFSET(0));

    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(4, GL_FLOAT, 0, BUFFER_OFFSET(sizeof(GLfloat)*mesh.m_vertices.size()));

    glEnableClientState(GL_NORMAL_ARRAY);
    glNormalPointer(GL_FLOAT, 0, BUFFER_OFFSET(sizeof(GLfloat)*mesh.m_vertices.size() + sizeof(GLfloat)*mesh.m_color.size()));

    glDrawArrays(GL_TRIANGLES, 0, mesh.m_vertices.size() / 3);

    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void RenderSystem::storeInVAO(Mesh & mesh){

}

void RenderSystem::render_vao(const Mesh & mesh) const{
    glBindVertexArray(mesh.m_vao);
    glDrawArrays(GL_TRIANGLES, 0, mesh.m_vertices.size() / 3);
    glBindVertexArray(0);
}

void RenderSystem::setRenderMode(const RenderMode m){
    renderMode = m;
}

void RenderSystem::switchRenderMode(){
    if(renderMode >= RenderMode::vao) renderMode = RenderMode::immediate;
    else {
        unsigned int mode = renderMode;
        mode++;
        renderMode = (RenderMode)mode;
    }

    switch(renderMode){
        case RenderMode::immediate:
        std::cout << "Immediate" << std::endl;
        break;
        case RenderMode::displayList:
        std::cout << "Display List" << std::endl;
        break;
        case RenderMode::vertexArray:
        std::cout << "Vertex Array" << std::endl;
        break;
        case RenderMode::vbo:
        std::cout << "VBO" << std::endl;
        break;
        case RenderMode::vao:
        std::cout << "VAO" << std::endl;
        break;
    }
}

void RenderSystem::setCamera(const Camera & camera) const{
    gluLookAt(camera.m_position.x, camera.m_position.y, camera.m_position.z,
    camera.m_target.x, camera.m_target.y, camera.m_target.z,
    camera.m_vectorV.x, camera.m_vectorV.y, camera.m_vectorV.z);
}

void RenderSystem::update(){
    static unsigned int frame{0};

    
   auto begin = std::chrono::high_resolution_clock::now();

	auto & mouv_data = r_entityFactory.getMouvement();
	auto & mesh_data = r_entityFactory.getMesh();
    auto & camera_data = r_entityFactory.getCamera();

    if(rsm.updated && rsm.switchRenderMode){
         switchRenderMode();
         rsm.updated = false;
         rsm.switchRenderMode = false;
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    setCamera(r_entityFactory.getCamera());

    glColor3f(1, 1, 1);
    glBegin(GL_LINES);
    glVertex3f(rsm.m_start.x, rsm.m_start.y, rsm.m_start.z);
    glVertex3f(rsm.m_end.x, rsm.m_end.y, rsm.m_end.z);
    glEnd();

    switch(renderMode){
        case RenderMode::immediate:{
            for(Entity & e : r_entityFactory.getEntities()){
                glPushMatrix();
                glm::vec3 & position = mouv_data[e.getMap()[cmp_Mouvement]].m_position;
                glTranslatef(position.x, position.y, position.z);
                render_immediate(mesh_data[e.getMap()[cmp_Mesh]]);
                glPopMatrix();
            }
        break;
        }
        case RenderMode::displayList:{
        	for(Entity & e : r_entityFactory.getEntities()){
                glPushMatrix();
                glm::vec3 & position = mouv_data[e.getMap()[cmp_Mouvement]].m_position;
                glTranslatef(position.x, position.y, position.z);
		        render_displayList(mesh_data[e.getMap()[cmp_Mesh]]);
                glPopMatrix();
	        }
        break;
        }
        case RenderMode::vertexArray:{
            for(Entity & e : r_entityFactory.getEntities()){
                glPushMatrix();
                glm::vec3 & position = mouv_data[e.getMap()[cmp_Mouvement]].m_position;
                glTranslatef(position.x, position.y, position.z);
		        render_vertexArray(mesh_data[e.getMap()[cmp_Mesh]]);
                glPopMatrix();
            }
        break;
        }
        case RenderMode::vbo:{
            for(Entity & e : r_entityFactory.getEntities()){
                glPushMatrix();
                glm::vec3 & position = mouv_data[e.getMap()[cmp_Mouvement]].m_position;
                glTranslatef(position.x, position.y, position.z);
		        render_vbo(mesh_data[e.getMap()[cmp_Mesh]]);
                glPopMatrix();
            }
        break;
        }
        case RenderMode::vao:{
            for(Entity & e : r_entityFactory.getEntities()){
                glPushMatrix();
                glm::vec3 & position = mouv_data[e.getMap()[cmp_Mouvement]].m_position;
                glTranslatef(position.x, position.y, position.z);
		        render_vao(mesh_data[e.getMap()[cmp_Mesh]]);
                glPopMatrix();
            }
        break;
        }
    }

    auto end = std::chrono::high_resolution_clock::now();

    std::cout << "temps de rendu : " << std::chrono::duration<double, std::milli>(end - begin).count() << " ms" << std::endl;

    //m_context->display();
}
