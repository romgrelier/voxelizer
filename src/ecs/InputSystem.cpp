#include "../../include/ecs/InputSystem.h"

extern RenderSystemMessage rsm;
extern NavigationSystemMessage nsm;

InputSystem::InputSystem(EntityFactory & entityFactory, Engine & engine, sf::Window * w) : 
    System(entityFactory, engine), 
    m_window{w},
    m_id_selection{0}{

}

void InputSystem::update(){
    time_t b = clock();
    sf::Event event;
    while (m_window->pollEvent(event))
    {
        switch(event.type){
        case sf::Event::Closed:
            m_window->close();
            break;
        case sf::Event::Resized:
            glViewport(0, 0, event.size.width, event.size.height);

            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            gluPerspective(90.0f, (GLfloat)640/(GLfloat)480, 0.1, 2000);

            glMatrixMode(GL_MODELVIEW);
            break;
        
        case sf::Event::MouseButtonPressed:
            if (sf::Mouse::isButtonPressed(sf::Mouse::Left)){
                sf::Vector2i localPosition = sf::Mouse::getPosition(*m_window);

                GLdouble matModelView[16], matProjection[16]; 
                GLint viewport[4];
                myVec3 m_start, m_end;

                // get matrix and viewport:
                glGetDoublev( GL_MODELVIEW_MATRIX, matModelView ); 
                glGetDoublev( GL_PROJECTION_MATRIX, matProjection ); 
                glGetIntegerv( GL_VIEWPORT, viewport ); 

                // window pos of mouse, Y is inverted on Windows
                GLfloat winX = (GLfloat)(localPosition.x); 
                GLfloat winY = (GLfloat)viewport[3] - (GLfloat)(localPosition.y);

                // get point on the 'near' plane (third param is set to 0.0)
                gluUnProject(winX, winY, 0.0, matModelView, matProjection, viewport, &rsm.m_start.x, &rsm.m_start.y, &rsm.m_start.z); 

                // get point on the 'far' plane (third param is set to 1.0)
                gluUnProject(winX, winY, 1.0, matModelView, matProjection, viewport, &rsm.m_end.x, &rsm.m_end.y, &rsm.m_end.z); 

                // now you can create a ray from m_start to m_end
                std::cout << "=target | x : " << rsm.m_end.x << " | y : " << rsm.m_end.y << " | z : " << rsm.m_end.z << std::endl;
            }
        break;
    
        case sf::Event::KeyPressed:
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)){
                r_entityFactory.getCamera().m_position.z-=0.5;
                r_entityFactory.getCamera().m_target.z-=0.5;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::D)){
                r_entityFactory.getCamera().m_position.x-=0.5;
                r_entityFactory.getCamera().m_target.x-=0.5;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Z)){
                r_entityFactory.getCamera().m_position.z+=0.5;
                r_entityFactory.getCamera().m_target.z+=0.5;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q)){
                r_entityFactory.getCamera().m_position.x+=0.5;
                r_entityFactory.getCamera().m_target.x+=0.5;
            }

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
                r_entityFactory.getCamera().m_position.y++;
                r_entityFactory.getCamera().m_target.y++;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
                r_entityFactory.getCamera().m_position.y--;
                r_entityFactory.getCamera().m_target.y--;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
                r_entityFactory.getCamera().m_position.x--;
                r_entityFactory.getCamera().m_target.x++;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
                r_entityFactory.getCamera().m_position.x++;
                r_entityFactory.getCamera().m_target.x--;
            }

            // Unit
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Tab)){
                m_id_selection++;
                if(m_id_selection >= r_entityFactory.getEntities().size()){
                    m_id_selection = 0;
                }
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::I)){
                auto & mouv_data = r_entityFactory.getMouvement();
                //glm::vec3 & direction = mouv_data[r_entityFactory.getEntities()[m_id_selection].getMap()[cmp_Mouvement]].m_direction;
                //direction.z+=0.5;
                glm::vec3 & target = mouv_data[r_entityFactory.getEntities()[m_id_selection].getMap()[cmp_Mouvement]].m_target;
                glm::vec3 & position = mouv_data[r_entityFactory.getEntities()[m_id_selection].getMap()[cmp_Mouvement]].m_position;
                if(position.z + 1 < CHUNK_SIZE_Z) target.z+=1;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::L)){
                auto & mouv_data = r_entityFactory.getMouvement();
                //glm::vec3 & direction = mouv_data[r_entityFactory.getEntities()[m_id_selection].getMap()[cmp_Mouvement]].m_direction;
                //direction.x-=0.5;
                glm::vec3 & target = mouv_data[r_entityFactory.getEntities()[m_id_selection].getMap()[cmp_Mouvement]].m_target;
                glm::vec3 & position = mouv_data[r_entityFactory.getEntities()[m_id_selection].getMap()[cmp_Mouvement]].m_position;
                if(position.x - 1 > 0) target.x-=1;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::K)){
                auto & mouv_data = r_entityFactory.getMouvement();
                //glm::vec3 & direction = mouv_data[r_entityFactory.getEntities()[m_id_selection].getMap()[cmp_Mouvement]].m_direction;
                //direction.z-=0.5;
                glm::vec3 & target = mouv_data[r_entityFactory.getEntities()[m_id_selection].getMap()[cmp_Mouvement]].m_target;
                glm::vec3 & position = mouv_data[r_entityFactory.getEntities()[m_id_selection].getMap()[cmp_Mouvement]].m_position;
                if(position.z - 1 > 0) target.z-=1;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::J)){
                auto & mouv_data = r_entityFactory.getMouvement();
                //glm::vec3 & direction = mouv_data[r_entityFactory.getEntities()[m_id_selection].getMap()[cmp_Mouvement]].m_direction;
                //direction.x+=0.5;
                glm::vec3 & target = mouv_data[r_entityFactory.getEntities()[m_id_selection].getMap()[cmp_Mouvement]].m_target;
                glm::vec3 & position = mouv_data[r_entityFactory.getEntities()[m_id_selection].getMap()[cmp_Mouvement]].m_position;
                if(position.x + 1 < CHUNK_SIZE_X) target.x+=1;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
                auto & weapon_data = r_entityFactory.getWeapon();
                weapon_data[r_entityFactory.getEntities()[m_id_selection].getMap()[cmp_weapon]].m_isFiring = true;

                nsm.updateAll = true;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::A)){
                auto & weapon_data = r_entityFactory.getWeapon();
                weapon_data[r_entityFactory.getEntities()[m_id_selection].getMap()[cmp_weapon]].m_targetId++;
                if(weapon_data[r_entityFactory.getEntities()[m_id_selection].getMap()[cmp_weapon]].m_targetId++ >= r_entityFactory.getEntities().size()){
                    weapon_data[r_entityFactory.getEntities()[m_id_selection].getMap()[cmp_weapon]].m_targetId = 0;
                }
            }

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::M)){
                //r_engine.getSystem(sys_render)->switchRenderMode();
                rsm.updated = true;
                rsm.switchRenderMode = true;
            }
            break;
        }
    }
    time_t e = clock();
    //std::cout << "=System - Input - update - time : " << ( (e - b) * 1000.0 ) / CLOCKS_PER_SEC << std::endl;
}