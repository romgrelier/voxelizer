#include "../include/Block.h"

glm::vec4 Block::getColor(const BlockType b){
    switch(b){
    case BlockType::grass:
        return glm::vec4(0.1, 0.3, 0, 1);
        break;
    case BlockType::dirt:
        return glm::vec4(0.7, 0.5, 0.2, 1);
        break;
    case BlockType::stone:
        return glm::vec4(0.5, 0.5, 0.5, 1);
        break;
    case BlockType::sand:
        return glm::vec4(0.7, 0.5, 0, 1);
    break;
    case BlockType::wood:
        return glm::vec4(1, 0.5, 0, 1);
        break;
    case BlockType::water:
        return glm::vec4(1, 0, 0, 1);
        break;
    default:
        return glm::vec4(0.2, 0.6, 0.4, 1);
        break;
    }
}
